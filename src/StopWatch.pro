QT       += core gui multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StopWatch
TEMPLATE = app

SOURCES += hcpp/main.cpp \
    hcpp/stopwatch.cpp \
    hcpp/about.cpp

HEADERS  += \
    hcpp/stopwatch.h \
    hcpp/about.h

FORMS    += \
    ui/stopwatch.ui \
    ui/about.ui

RESOURCES += \
    resource/resources.qrc

win32 {
    RC_ICONS = ..\bundling-resources\windows\icons.ico
}
