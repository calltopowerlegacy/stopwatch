#ifndef ABOUT_H
#define ABOUT_H

#include <QDialog>
#include <QString>

namespace Ui {
class About;
}

class About : public QDialog
{
    Q_OBJECT

public:
    explicit About(QWidget *parent = 0, QString _version = "1.0 build 1");
    ~About();
    void setVersion(QString _version);

private:
    Ui::About *ui;
    QString version;

private slots:
    void close();
};

#endif // ABOUT_H
