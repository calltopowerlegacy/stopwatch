#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <QMainWindow>
#include <QLCDNumber>
#include <QString>
#include <QTimer>
#include "about.h"

namespace Ui {
class StopWatch;
}

class StopWatch : public QMainWindow
{
    Q_OBJECT

public:
    explicit StopWatch(QWidget *parent = 0);
    ~StopWatch();

private:
    Ui::StopWatch *ui;
    About *aboutUI;
    QTimer *timer;
    bool isRunning;
    bool isStopped;
    bool clearWasClicked;
    int timerUpdate;
    unsigned long milliseconds;
    QString from_milliseconds(unsigned long duration);
    QString get_milliseconds(unsigned long duration);
    void setTime();
    QString getCurrentDir();
    void playSoundEnd();
    void playSoundSplit();
    void playSoundClear();
    void playSoundDot();

private slots:
    void startStop();
    void reset();
    void checkState();
    void showTime();
    void split();
    void clearClicked();
    void clearSplitList();
    void showAbout();
};

#endif // STOPWATCH_H
