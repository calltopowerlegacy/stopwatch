#include "stopwatch.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    StopWatch sw;
    sw.show();

    return a.exec();
}
