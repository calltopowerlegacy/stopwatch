#include "about.h"
#include "ui_about.h"

About::About(QWidget *parent, QString _version) :
    QDialog(parent),
    ui(new Ui::About),
    version(_version)
{
    ui->setupUi(this);
    ui->label_version->setText("Version " + version);

    connect(ui->button_ok, SIGNAL(clicked()), this, SLOT(close()));
}

About::~About()
{
    delete ui;
}

void About::setVersion(QString _version)
{
    version = _version;
    ui->label_version->setText("Version: " + version);
}

void About::close()
{
    this->setVisible(false);
}
