#include "stopwatch.h"
#include "ui_stopwatch.h"
#include <QTimer>
#include <QDateTime>
#include <QDir>
#include <QSound>

StopWatch::StopWatch(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StopWatch),
    isRunning(false),
    isStopped(true),
    clearWasClicked(false),
    timerUpdate(5),
    milliseconds(0)
{
    ui->setupUi(this);

    aboutUI = new About(this, "1.2 build 1");

    QApplication::setWindowIcon(QIcon(":/resources/img/logo"));

    ui->button_startStop->setEnabled(true);
    ui->button_reset->setEnabled(true);
    ui->checkBox->setEnabled(true);
    ui->checkBox->setChecked(false);
    ui->timeEdit->setEnabled(false);
    ui->spinBox->setEnabled(false);
    ui->button_split->setEnabled(false);
    ui->button_clear->setEnabled(false);

    timer = new QTimer(this);
    connect(ui->action_about, SIGNAL(triggered()), this, SLOT(showAbout()));
    connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
    connect(ui->button_startStop, SIGNAL(clicked()), this, SLOT(startStop()));
    connect(ui->button_reset, SIGNAL(clicked()), this, SLOT(reset()));
    connect(ui->checkBox, SIGNAL(clicked()), this, SLOT(checkState()));
    connect(ui->button_split, SIGNAL(clicked()), this, SLOT(split()));
    connect(ui->button_clear, SIGNAL(clicked()), this, SLOT(clearClicked()));

    showTime();
}

StopWatch::~StopWatch()
{
    delete ui;
    if(aboutUI)
    {
        delete aboutUI;
    }
}

void StopWatch::checkState()
{
    ui->timeEdit->setEnabled(ui->checkBox->isChecked() && ui->checkBox->isEnabled());
    ui->spinBox->setEnabled(ui->checkBox->isChecked() && ui->checkBox->isEnabled());
}

QString StopWatch::from_milliseconds(unsigned long duration)
{
    QString res;
    duration /= 1000;
    int seconds = (int) (duration % 60);
    duration /= 60;
    int minutes = (int) (duration % 60);
    duration /= 60;
    int hours = (int) (duration % 24);

    return res.sprintf("%02d:%02d:%02d", hours, minutes, seconds);
}

QString StopWatch::get_milliseconds(unsigned long duration)
{
    QString res;
    int milliseconds = (int) (duration % 1000);
    return res.sprintf("%03d", milliseconds);
}

void StopWatch::startStop()
{
    if(isStopped)
    {
        if(ui->checkBox->isChecked())
        {
            unsigned long tmp_milliseconds = 0;
            tmp_milliseconds += ui->timeEdit->time().hour() * 60 * 60 * 1000;
            tmp_milliseconds += ui->timeEdit->time().minute() * 60 * 1000;
            tmp_milliseconds += ui->timeEdit->time().second() * 1000;
            tmp_milliseconds += ui->spinBox->value();

            if(tmp_milliseconds <= 0)
            {
                ui->statusBar->showMessage("Please set up a countdown.");
            } else {
                ui->button_startStop->setEnabled(true);
                ui->button_startStop->setText("Stop");
                ui->checkBox->setEnabled(false);
                ui->button_split->setEnabled(true);
                ui->button_clear->setEnabled(true);
                checkState();
                if(!isRunning)
                {
                    milliseconds = tmp_milliseconds;
                    clearSplitList();
                }
                timer->start(timerUpdate);
                setTime();
                isRunning = true;
                isStopped = false;
                ui->statusBar->showMessage("Countdown timer started");
            }
        } else
        {
            ui->button_startStop->setEnabled(true);
            ui->button_startStop->setText("Stop");
            ui->checkBox->setEnabled(false);
            ui->button_split->setEnabled(true);
            ui->button_clear->setEnabled(true);
            if(!isRunning)
            {
                clearSplitList();
            }
            checkState();
            timer->start(timerUpdate);
            isRunning = true;
            isStopped = false;
            ui->statusBar->showMessage("Timer started");
        }
    } else
    {
        isStopped = true;
        ui->button_startStop->setEnabled(true);
        ui->button_startStop->setText("Continue");
        ui->button_split->setEnabled(true);
        ui->button_clear->setEnabled(true);
        if(milliseconds == 0)
        {
            ui->checkBox->setEnabled(true);
        }
        checkState();
        timer->stop();
        ui->statusBar->showMessage("Timer stopped");
    }
}

void StopWatch::reset()
{
    isRunning = false;
    isStopped = true;
    ui->button_startStop->setEnabled(true);
    ui->button_startStop->setText("Start");
    ui->checkBox->setEnabled(true);
    ui->button_split->setEnabled(false);
    ui->button_clear->setEnabled(true);
    checkState();
    timer->stop();
    milliseconds = 0;
    setTime();
    ui->statusBar->showMessage("Timer stopped. Reset.");
}

void StopWatch::setTime()
{
    try {
        ui->lcdNumber_seconds->display(from_milliseconds(milliseconds));
        ui->lcdNumber_ms->display(get_milliseconds(milliseconds));
    } catch(int e) {
        reset();
        ui->statusBar->showMessage("An error occurred. Timer stopped. Reset.");
    }
}

QString StopWatch::getCurrentDir()
{
    QDir dir(QCoreApplication::applicationDirPath());
#if defined(Q_OS_MAC)
    if (dir.dirName() == "MacOS")
    {
        dir.cdUp();
    }
#endif

    QString dir_str = dir.absolutePath();
#if defined(Q_OS_MAC)
    dir_str = dir_str.endsWith(QDir::separator()) ? dir_str : QString("%1%2%3").arg(dir_str).arg(QDir::separator()).arg("Resources");
#endif
    return dir_str;
}

void StopWatch::playSoundEnd()
{
    QSound::play(":/resources/audio/alarm");
}

void StopWatch::playSoundSplit()
{
    QSound::play(":/resources/audio/split");
}

void StopWatch::playSoundClear()
{
    QSound::play(":/resources/audio/clear");
}

void StopWatch::playSoundDot()
{
    QSound::play(":/resources/audio/dot");
}

void StopWatch::showTime()
{
    if(ui->checkBox->isChecked())
    {
        if(milliseconds > 0)
        {
            if(milliseconds > (unsigned long) timerUpdate)
            {
                milliseconds -= timerUpdate;
            } else
            {
                milliseconds = 0;
            }
            bool exp1 = (milliseconds >= (unsigned long) (1000 - timerUpdate)) && (milliseconds <= (unsigned long) (1000 + timerUpdate));
            bool exp2 = (milliseconds >= (unsigned long) (2000 - timerUpdate)) && (milliseconds <= (unsigned long) (2000 + timerUpdate));
            bool exp3 = (milliseconds >= (unsigned long) (3000 - timerUpdate)) && (milliseconds <= (unsigned long) (3000 + timerUpdate));
            if(exp1 || exp2 || exp3)
            {
                playSoundDot();
            } else if(milliseconds == 0)
            {
                playSoundEnd();
            }
            setTime();
        } else
        {
            reset();
            ui->statusBar->showMessage("Countdown ended.");
        }
    } else
    {
        setTime();
        milliseconds += timerUpdate;
    }
}

void StopWatch::clearClicked()
{
    clearWasClicked = true;
    clearSplitList();
}

void StopWatch::clearSplitList()
{
    if(ui->listWidget->count() > 0)
    {
        if(isRunning || clearWasClicked)
        {
            clearWasClicked = false;
            playSoundClear();
        }
        ui->listWidget->clear();
        ui->statusBar->showMessage("Cleared the split list.");
    } else
    {
        ui->statusBar->showMessage("The split list does not contain any items.");
    }
}

void StopWatch::split()
{
    QString formattedSeconds(from_milliseconds(milliseconds) + " " + get_milliseconds(milliseconds - (ui->checkBox->isChecked() ? 0 : timerUpdate)));
    bool found = false;
    for(int i = 0; i < ui->listWidget->count(); ++i)
    {
        if(ui->listWidget->item(i)->text() == formattedSeconds)
        {
            found = true;
            break;
        }
    }
    if(found)
    {
        ui->statusBar->showMessage("The time '" + formattedSeconds + "' has already been added.");
    } else {
        playSoundSplit();
        ui->statusBar->showMessage("Added time '" + formattedSeconds + "' to the split list.");
        new QListWidgetItem(formattedSeconds, ui->listWidget);
    }
}

void StopWatch::showAbout()
{
    aboutUI->show();
}
