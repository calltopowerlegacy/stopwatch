Stop watch
======

A simple stop watch.

Copyright 2014 Denis Meyer

Version
-------
1.2 build 1

Images
------

Logo icon: Copyright (c) Icons8, License: Linkware, Commercial usage: Allowed, Creative Commons Attribution-NoDerivs 3.0 Unported, URL: http://icons8.com
